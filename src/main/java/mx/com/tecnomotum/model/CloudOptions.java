package mx.com.tecnomotum.model;

import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.options.PipelineOptions;

public interface CloudOptions extends PipelineOptions {
    @Description("The Google Cloud project ID for the Cloud Bigtable instance.")
    String getBigtableProjectId();

    void setBigtableProjectId(String bigtableProjectId);

    @Description("The Google Cloud Bigtable instance ID .")
    String getBigtableInstanceId();

    void setBigtableInstanceId(String bigtableInstanceId);

    @Description("The Cloud Bigtable table ID in the instance." )
    String getBigtableTableId();

    void setBigtableTableId(String bigtableTableId);

    @Description("The Cloud Bigtable profile ID for this app." )
    String getProfileId();

    void setProfileId(String profileId);

    @Description("The PubSub subscriber id to take the messages from." )
    String getSubscriberId();

    void setSubscriberId(String subscriberId);

    @Description("The topic id to publish the processed messages" )
    String getTopicId();

    void setTopicId(String topicId);
}
