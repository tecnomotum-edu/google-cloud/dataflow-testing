package mx.com.tecnomotum;

import com.google.cloud.bigtable.beam.CloudBigtableConfiguration;
import com.google.cloud.bigtable.beam.CloudBigtableIO;
import com.google.cloud.bigtable.beam.CloudBigtableTableConfiguration;
import com.google.gson.Gson;
import mx.com.tecnomotum.model.CloudOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.hadoop.hbase.client.Mutation;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;

public class MainTest {
    public static void main (String args[]) {
        CloudOptions options;
        Pipeline pipeline;
        options = PipelineOptionsFactory.fromArgs(args).withValidation().as(CloudOptions.class);
        pipeline = Pipeline.create(options);

        CloudBigtableTableConfiguration config = new CloudBigtableTableConfiguration.Builder()
                .withProjectId("motum-mwv2-dev")
                .withInstanceId("final-destiny")
                .withTableId("writing-test")
                .build();

        pipeline.apply("Read pub sub", PubsubIO.readStrings().fromSubscription("projects/motum-mwv2-dev/subscriptions/dataflow-test"))

        .apply(ParDo.of(new DoFn<String, Mutation>() {
            @ProcessElement
            public void processElement(ProcessContext c){
                Gson gson = new Gson();
                SocketsServerMessage ssm = gson.fromJson(c.element(), SocketsServerMessage.class);

                if(ssm.getDeviceStatus() != null && ssm.getDeviceStatus().getIgnition() != null){
                    Put put = new Put(Bytes.toBytes(ssm.getSerialNumber()));
                    put.addColumn(Bytes.toBytes("sink"), Bytes.toBytes("gpsDate"), Bytes.toBytes(ssm.getGpsDate()));
                    c.output(put);
                    System.out.println("saved data: " + ssm.getSerialNumber());
                }
                else{
                    System.out.println("descartado");
                }
            }
        }))
           .apply("writing", CloudBigtableIO.writeToTable(config));

        pipeline.run();
    }
}
